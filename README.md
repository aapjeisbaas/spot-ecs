This cloudformation script was designed to be a **super low cost, scalable and resiliant** container hosting platform. 

### Prerequisites
 - VPC
 - Internet conectivity
 - Subnets (public and private prefered but not required)
 - Public ssh key in: `SSM: /publickeys/${username}` 
 - [Management Security Group](https://gitlab.com/aapjeisbaas/spot-vpn "Bastion / VPN example")
 - [Load Balancer Security Group](https://gitlab.com/aapjeisbaas/gulb "Grand Unified Load Balancer") (if a full ELB is to rich for your budget you can use some other group to access your containers)
 - [Deployer user + cloudformation role](https://gitlab.com/aapjeisbaas/sysop-tools/blob/master/iam.yml) 

#### How to deploy
You can sumodule this repo in your own infrastructure repo:
```
git submodule add https://gitlab.com/aapjeisbaas/spot-ecs.git
# For alb you can roll your owl or plug and play:
git submodule add  https://gitlab.com/aapjeisbaas/gulb.git
```
To deploy the cluster add the following to you `.gitlab-ci.yml`
```
cd spot-ecs
./deploy.sh
cd ..

# if using gulb:
cd gulb
./deploy.sh
cd ..
```
Also it needs the following env vars:
 - AWS_ACCESS_KEY_ID
 - AWS_DEFAULT_REGION
 - AWS_SECRET_ACCESS_KEY
 - AdminSecurityGroups
 - AppSubnetName
 - InstanceType
 - LbSecurityGroups
 - StackName
 - Vpc

Auto deploy some services on that cluster, add a line to `containers.vars`
```
{"ContainerName": "test", "ContainerCluster": "container-cluster", "ContainerCpu": "10", "ContainerMemory": "64", "ContainerPort": "8080", "ListenerPriority": "1337", "TaskDefinition": "[TaskArn]", "WebPath": "*", "WebDomain": "*", "Role_Arn": "[CloudformationArn]", "Protocol": "HTTP"}
```

in `.gitlab-ci.yml`:
```
accountId=$(aws sts get-caller-identity | jq '.Account' -r)
CloudFormationArn="arn:aws:iam::$accountId:role/Cloudformation"

cp spot-ecs/container-services.yml .

echo "deploy docker/ecs services and target groups"
cat container.vars| while read line; do
  ContainerName=$(echo $line | jq ".ContainerName" -r)
  echo "Deploying container: $ContainerName "
  TaskArn=$(aws ecs describe-task-definition --task $ContainerName | jq ".taskDefinition .taskDefinitionArn" -r)
  if [ -z $TaskArn ]; then # check if there is a task def if not; create log group and dummy task def
    aws logs create-log-group --log-group-name $ContainerName
    aws ecs register-task-definition --family $ContainerName --container-definitions $(cat example-task.json | jq -c . | sed "s/\[ContainerName\]/${ContainerName}/g")
    sleep 20
    TaskArn=$(aws ecs describe-task-definition --task $ContainerName | jq ".taskDefinition .taskDefinitionArn" -r)
  fi
  EscapedTaskArn=$(echo $TaskArn | sed 's/\//\\\//g') 
  EscapedCloudformationArn=$(echo $CloudFormationArn | sed 's/\//\\\//g')
  vars_json=$(echo $line | sed "s/\[TaskArn\]/${EscapedTaskArn}/g; s/\[CloudformationArn\]/${EscapedCloudformationArn}/g")
  ansible-playbook spot-ecs/container-services-ansible.yml --extra-vars "$vars_json" -vvvvv >> container-service-$ContainerName-ansible.log
  EXIT=$(($EXIT + $?))
  echo "$EXIT"
done
```

Have a look over here to see it in action: [Pipelines](https://gitlab.com/aapjeisbaas/spot-ecs/pipelines)

### Now what...
There are several routes from here forward:
 - Push something to the Amazon Elastic Container Registry.
 - Try out the [GitLab Container Registry](https://gitlab.com/help/user/project/container_registry)
 - Create a [task definition](https://console.aws.amazon.com/ecs/home?/taskDefinitions/create&#/taskDefinitions/create) by hand and turn it into a service.
 - Get an automated process going to automaticly ship container versions trough testing stages and into production.

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fspot-ecs%2FREADME.md&dt=spot-ecs)

