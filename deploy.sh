#!/bin/bash

# Get the cloudformation arn, this is a required var for ansible to be able to switch role and deploy stacks.
# Infra requirements:
# CI/CD User in the account
# Cloudformation role in the account
# "Application / Backend" Subnets with a Name tag (name to be placed in: $AppSubnetName)

# Vars:
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_DEFAULT_REGION
# AppSubnetName
# StackName
# InstanceType
# AdminSecurityGroups
# Vpc
# LbSecurityGroups

accountId=$(aws sts get-caller-identity | jq '.Account' -r)
CloudFormationRoleArn="arn:aws:iam::$accountId:role/Cloudformation"
ApplicationSubnets=$(aws ec2 describe-subnets --filters Name=tag:Name,Values=$AppSubnetName --output json | jq '[.Subnets[] .SubnetId] | @csv' -r | sed 's/","/,/g')

# to keep track of exit codes
EXIT=0

# get the latest ami
ImageId=$(aws ec2 describe-images --owners amazon --filters "Name=name,Values=*amazon-ecs-optimized" --query 'sort_by(Images, &CreationDate)[-1].ImageId')

echo "deploy container cluster related cloudformation using these vars:"
vars_json='{"CloudFormationRoleArn": "'$CloudFormationRoleArn'", "StackName": "'$StackName'", "ApplicationSubnets": '$ApplicationSubnets', "InstanceType": "'$InstanceType'", "AdminSecurityGroups": "'$AdminSecurityGroups'", "Vpc": "'$Vpc'", "LbSecurityGroups": "'$LbSecurityGroups'", "ImageId": '$ImageId'}'
echo "$vars_json"
echo "$vars_json" | jq '.'
ansible-playbook spot-ecs-ansible.yml --extra-vars "$vars_json" -vvvvv >> $StackName-ansible.log
EXIT=$(($EXIT + $?)) ; echo "$EXIT"


exit $EXIT
