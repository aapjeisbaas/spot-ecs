#!/bin/bash
PATH=/usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin:/usr/local/bin

export AWS_DEFAULT_REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq '.region' -r)

# This script logs metrics about the docker instances to cloudwatch.
# Written by: Stein van Broekhoven 01-04-2016
# Last updated: 13-04-2018

##########
## VARS ##
##########

# Get loacl ID
EC2_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

# percentage of disk usage before sending data to cloudwatch
ERROR=50
EC2_SCALING_GROUP=$(aws ec2 describe-instances --instance-ids $EC2_ID | jq '.Reservations[] .Instances[] .Tags[] | select(.Key=="aws:autoscaling:groupName") .Value' -r)
ECS_CLUSTER=$(cat /usr/local/bin/cluster-name)

# Create docker id list without ecs-agent
INSTANCES=$(docker ps |awk '{print $1}' |grep -ve "CONTAINER" | grep -ve "$(docker ps --filter="name=ecs-agent" | grep -ve "CONTAINER" | awk '{print $1}')")


# Get Running docker instances count
RUN_COUNT=$(echo $INSTANCES |wc -w)


######################
## Sending data out ##
######################

# Log empty vs in use docker host to cloudwatch and protect from scalein if in use
if [ $RUN_COUNT -eq 0 ] ; then
  aws autoscaling set-instance-protection --instance-ids $EC2_ID --auto-scaling-group-name $EC2_SCALING_GROUP --no-protected-from-scale-in
  aws cloudwatch put-metric-data --metric-name EmptyServer --namespace $ECS_CLUSTER --value 1
else
  aws autoscaling set-instance-protection --instance-ids $EC2_ID --auto-scaling-group-name $EC2_SCALING_GROUP --protected-from-scale-in
  aws cloudwatch put-metric-data --metric-name EmptyServer --namespace $ECS_CLUSTER --value 0
fi

# Log disk percentage usage is a usage is over $ERROR threshold
for INSTANCE in $INSTANCES ; do
  # get percentage of disk usage on / in docker INSTANCE
  PERCENT=$(echo $(docker exec $INSTANCE /bin/df / --output=pcent) |sed 's/ //g; s/Use//g; s/%//g; s/ //g'; )
  if [ $PERCENT -gt $ERROR ]; then
    aws cloudwatch put-metric-data --metric-name HighestImageStorageUsagePercentage --namespace DockerService --value $PERCENT
    echo "error image: $INSTANCE disk usage at: $PERCENT %"
  fi
done
