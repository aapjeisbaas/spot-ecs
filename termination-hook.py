#!/usr/bin/env python3
import boto3
import time
import json
import re
from subprocess import call
import urllib.request


def instanceArnList():
    # returns container instance arn list
    response = EcsClient.list_container_instances(
        cluster=clusterName,
        maxResults=100,
    )
    return response['containerInstanceArns']

def containerInstanceInfoList():
    # returns info about container instances
    response = EcsClient.describe_container_instances(
        cluster=clusterName,
        containerInstances=instanceArnList()
    )
    return response['containerInstances']

def getLocalInstanceId():
    f = urllib.request.urlopen('http://169.254.169.254/latest/meta-data/instance-id')
    instance = f.read().decode('utf-8')
    return instance

def drain(instanceArn):
    response = EcsClient.update_container_instances_state(
        cluster=clusterName,
        containerInstances=[
            instanceArn,
        ],
        status='DRAINING'
    )

def drainSelf():
    localInstanceId = getLocalInstanceId()
    for instance in containerInstanceInfoList():
        if instance['ec2InstanceId'] == localInstanceId:
            drain(instance['containerInstanceArn'])


# create a boto3 client
SqsClient = boto3.client('sqs')

# create a boto3 client
EcsClient = boto3.client('ecs')

# retrieve container-cluster-name
config = open('/etc/ecs/ecs.config', 'r').read()
clusterName = re.match( r'(ECS_CLUSTER=)(.*)', config).group(2)
# get the queue that contains the ecs clustername
queues = SqsClient.list_queues(QueueNamePrefix=clusterName)
queue_url = queues['QueueUrls'][0]

# local ec2 arn:
f = urllib.request.urlopen('http://169.254.169.254/latest/meta-data/placement/availability-zone')
az = f.read().decode('utf-8')
instanceId = getLocalInstanceId()
arn = 'arn:aws:ec2:' + az + ':instance/' + instanceId

# main loop
while True:
    time.sleep(5)
    messages = SqsClient.receive_message(QueueUrl=queue_url, MaxNumberOfMessages=5, VisibilityTimeout=0)
    if 'Messages' in messages:
        for message in messages['Messages']: # 'Messages' is a list
            # process the messages
            body = json.loads(message['Body'])
            try:
                for resource in body['resources']:
                    if resource == arn:
                        instanceAction = body['detail']['instance-action']
                        if instanceAction == 'terminate':
                            drainSelf()
                        SqsClient.delete_message(QueueUrl=queue_url,ReceiptHandle=message['ReceiptHandle'])
            except:
                pass
            try:
                if body['detail']['EC2InstanceId'] == instanceId:
                    lifecycleTransition = body['detail']['LifecycleTransition']
                    if lifecycleTransition == 'autoscaling:EC2_INSTANCE_TERMINATING':
                        drainSelf()
                    SqsClient.delete_message(QueueUrl=queue_url,ReceiptHandle=message['ReceiptHandle'])
            except:
                pass



        
